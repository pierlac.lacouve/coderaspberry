<?php include "../controller/auto-import.php"; ?>
<!DOCTYPE html>
<html lang="fr">
<?php include "../controller/head.html"; ?>
<body>
<section id="container" >
    <?php
    include "../controller/header.html";
    include "../controller/aside.html";
    ?>

    <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-9 main-chart">
                    <div class="col-md-12 col-sm-12 mb">
                        <div class="row">
                            <h1>Git Tree</h1>
                            <pre><?php
                                //print(shell_exec("git log --all --decorate --oneline --graph 2>&1"));
                                print(shell_exec("git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"));
                                ?>
                            </pre>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 ds">
                    <h3>Set Raspberry branch</h3>
                    <div class="desc">
                        <form class="col-lg-12" action="../controller/git-update.php" method="post">
                            <div class="col-lg-6">
                                <input class="btn btn-theme btn-block" type="submit" name="branch" value="master">
                            </div>
                            <div class="col-lg-6">
                                <input class="btn btn-theme btn-block" type="submit" name="branch" value="dev">
                            </div>
                        </form>
                    </div>
                    <hr>
                    <h3>Current Raspberry branch</h3>
                    <div class="desc">
                        <?php
                            $branch=shell_exec("git branch");
                            if($branch[0]!="*") {
                                $masterStyle = " bg-theme";
                                $devStyle = "";
                            }
                            else {
                                $masterStyle = "";
                                $devStyle = " bg-theme";
                            }
                            chdir("/home/pi/www-data/coderaspberry/php/view");
                        ?>
                        <div class="col-lg-6">
                            <span class="col-lg-12 badge<?php print $masterStyle;?>"><i class="fa fa-laptop"></i><p>Master</p></span>
                        </div>
                        <div class="col-lg-6">
                            <span class="col-lg-12 badge<?php print $devStyle;?>"><i class="fa fa-code"></i><p>Dev</p></span>
                        </div>
                    </div>
                </div>
        </section>
    </section>

    <!--main content end-->
    <?php include "../controller/footer.html"; ?>
</section>
<?php include "../controller/scripts.html"; ?>
</body>
</html>
<!-- end document-->
