<?php include "../controller/auto-import.php"; ?>
<!DOCTYPE html>
<html lang="fr">
<?php include "../controller/head.html"; ?>
<body>
<section id="container" >
    <?php
    include "../controller/header.html";
    include "../controller/aside.html";
    ?>

    <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-9 main-chart">
                    <div class="col-md-12 col-sm-12 mb">
                        <div class="row">

                        </div>
                    </div>
                </div>

                <div class="col-lg-3 ds">
                    <h3>Change PC Status</h3>
                    <div class="desc">
                        <div class="col-lg-12">
                            <div class="col-lg-4">
                                <button class="btn btn-theme btn-block" onclick="sendOrder('on')">
                                    <i class="fa fa-angle-up"></i>
                                </button>
                            </div>
                            <div class="col-lg-4">
                                <button class="btn btn-theme btn-block" onclick="sendOrder('off')">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                            <div class="col-lg-4">
                                <button class="btn btn-theme btn-block" onclick="sendOrder('offHard')">
                                    <i class="fa fa-angle-double-down"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3>Current PC Status</h3>
                    <div class="desc">
                            <?php
                            $pcStatus="Off";
                            $pcBadge="";
                            if($db->select(array("*"),array("pc-status"),array(),1,"date DESC")["state"]=="1"){
                                $pcStatus="On";
                                $pcBadge=" bg-theme";
                            }
                            ?>
                        <div class="col-lg-12">
                            <span id="spanOnOff" class="col-lg-6 col-lg-offset-3 badge<?php print($pcBadge); ?>">
                                <i class="fa fa-laptop"></i>
                                <p id="pOnOff"><?php print($pcStatus); ?></p>
                            </span>
                        </div>
                    </div>
                </div>
        </section>
    </section>

    <!--main content end-->
    <?php include "../controller/footer.html"; ?>
</section>
<?php include "../controller/scripts.html"; ?>
<script type="text/javascript">
    function sendOrder(order){
        $.ajax({
            url : '../controller/control-watch.php',
            type : 'POST',
            data : 'command='+order
        });
    };
    setInterval(function(){
        $.ajax({
            url : '../controller/fetch-pc-data.php',
            type : 'POST',
            success : function(data){
                var obj = jQuery.parseJSON(data);
                if(obj["state"]==="1"){
                    document.getElementById("spanOnOff").classList.add("bg-theme");
                    document.getElementById("pOnOff").innerText = "On";
                }
                else {
                    document.getElementById("spanOnOff").classList.remove("bg-theme");
                    document.getElementById("pOnOff").innerText = "Off";
                }
            }
        })
    }, 1000);

</script>
</body>
</html>
<!-- end document-->
