<?php
include "controller/auto-import.php";

chdir("/home/pi/www-data/coderaspberry/");

if(isset($_POST["branch"]) && ($_POST["branch"]=="master" || $_POST["branch"]=="dev")) {
    shell_exec("git fetch --all 2>&1");

    $currentBranch=explode("\n",str_replace(" ","",shell_exec("git branch 2>&1")));

    if(($currentBranch[1][0]=="*" && $_POST["branch"]=="dev") || ($currentBranch[0][0]=="*" && $_POST["branch"]=="master")){
       shell_exec("git checkout ".$_POST["branch"]." 2>&1");
    }
    shell_exec("git reset --hard origin/" . $_POST["branch"]);
}
header("Location:../view/git-manager.php");