<?php
include "auto-import.php";
if(isset($_POST["command"]) && ($_POST["command"]=="on" || $_POST["command"]=="off" || $_POST["command"]=="offHard")) {
    $db->insert("php-orders",array("order-time","command","execution-done"),array(date("Y-m-d H:i:s"),"PC ".$_POST["command"],"0"));

    if(isset($_SESSION["erreur"])){
        error_log(print_r($_SESSION["erreur"],true));
        unset($_SESSION["erreur"]);
    }
}
header("Location:../view/pc-manager.php");
