<?php


class database
{
	private $pdo;

	public

		/** Constructor
		 */
	function __construct()
	{
		try {
			$strConnection = 'mysql:host=localhost;dbname=database;charset=utf8';
			$this->pdo = new PDO ($strConnection, 'process_access', 'qlvUMEkEZJzVthKN');
		} catch (PDOException $e) {
			session_start();
			$_SESSION['erreur'][] = 'ERROR PDO dans ' . $e->getMessage();
			//header("Location:../index.php");
		}
	}

	/** Sends a select sql query
	 * @param $columns
	 * @param $tables
	 * @param array $conditions
	 * @param int $fetch
	 * @param string $orderBy
	 * @return bool|mixed|PDOStatement
	 */
	function select($columns, $tables, $conditions = array(), $fetch = 1, $orderBy = "", $maxrow = -1)
	{
		$columnsStr = implode(",",$this->format($columns,"`"));
		$tablesStr = implode(",",$this->format($tables,"`"));
		if (!isset($conditions[0]))
			$conditions = array(1);
		if ($orderBy != "")
			$orderBy = " ORDER BY " . $orderBy;
		if ($maxrow != -1)
			$maxrow = " LIMIT " . $maxrow;
		else
			$maxrow = "";
		$conditionsStr = " WHERE " . implode(" AND ", $conditions) . $orderBy . $maxrow;

		$req = $this->pdo->prepare("SELECT DISTINCT " . $columnsStr . " FROM " . $tablesStr . $conditionsStr . ";");
		//print("<p>SELECT " . $columnsStr . " FROM " . $tablesStr . $conditionsStr . ";</p>");
		$req->execute();

		if ($fetch == 1)
			return $req->fetch();
		else
			return $req;
	}

	/**
	 * @param $table
	 * @param $columns
	 * @param $valuesArr
	 * @param int $returnId
	 * @return string
	 */
	function insert($table, $columns, $valuesArr, $returnId = 0)
	{
		$columns = $this->format($columns,"`");
		$values = $this->format($valuesArr);
		$columnsStr = implode(",", $columns);
		$valuesStr = implode(",", $values);
		$reqStr = "INSERT INTO `" . $table . "` (" . $columnsStr . ") VALUES (" . $valuesStr . ");";

		$stmt = $this->pdo->prepare($reqStr);
		if (!$stmt->execute()) {
			$_SESSION["erreur"][] = "erreur : " . $stmt->errorInfo()[2] . " : " . $reqStr;
		}
		if ($returnId == 1) {
			$lii = $this->pdo->lastInsertId();
			return $lii;
		}
	}

	/** Updates database line
	 * @param $table
	 * @param $columns
	 * @param $valuesArr
	 * @param array $conditions
	 */
	function update($table, $columns, $valuesArr, $conditions = array(),$setNull=false)
	{
        $columns = $this->format($columns,"`");
		$values = $this->format($valuesArr);
		$reqStr = "UPDATE `" . $table . "` SET ";
		$update = array();
		for ($i = 0; $i < sizeof($columns); ++$i) {
			if ($values[$i] != "NULL" || $setNull)
				$update[] = $columns[$i] . "=" . $values[$i];
		}

		if (!isset($conditions[0]))
			$conditions = array(1);
		$reqStr = $reqStr . " " . implode(", ", $update) . " WHERE " . implode(" AND ", $conditions) . ";";
		$stmt = $this->pdo->prepare($reqStr);
		if (!$stmt->execute()) {
			$_SESSION["erreur"][] = "erreur : " . $stmt->errorInfo()[2] . " : " . $reqStr;
		}
	}

	/** Deletes lines from a specified table respecting the conditions
	 * @param $table
	 * @param array $conditions
	 */
	function delete($table, $conditions = array())
	{
		$reqStr = "DELETE FROM `" . $table . "` ";

		if (!isset($conditions[0]))
			$conditions = array(1);
		$reqStr = $reqStr . " WHERE " . implode(" AND ", $conditions) . ";";
		$stmt = $this->pdo->prepare($reqStr);
		if (!$stmt->execute()) {
			$_SESSION["erreur"][] = "erreur : " . $stmt->errorInfo()[2] . " : " . $reqStr;
		}
	}

	/** Adds a character at the extremities of all strings from an array (default is "'")
	 * @param array $data
	 * @param string $char
	 * @return array
	 */
	function format($data, $char = "'")
	{
		$dataFormat = array();
		foreach ($data as $str) {
			if ($str == "NULL" || $str == "*")
				array_push($dataFormat, $str);
			else
				array_push($dataFormat, $char . str_replace("'", "\'", $str) . $char);
		}
		return $dataFormat;
	}

    /**
     *
     */
	function reinitialize()
	{
		$this->pdo->prepare(file_get_contents("assets/pivot.sql"))->execute();
	}
}