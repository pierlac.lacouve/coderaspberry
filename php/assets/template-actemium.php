<?php
include "controller/auto-import.php";
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <!--HEAD IMPORT-->
    <?php include("../controller/head.html");?>
    <!-- Title Page-->
    <title>PageName</title>
</head>
<body class="animsition">
<!-- HEADER DESKTOP-->
<?php include("header.php") ?>
<!-- END HEADER DESKTOP -->
<div class="page-wrapper">
    <!-- PAGE CONTENT-->
    <div class="page-content--bgf7 spacer">
    </div>
</div>
<?php include ("../controller/scripts.html");?>
</body>
</html>
<!-- end document-->