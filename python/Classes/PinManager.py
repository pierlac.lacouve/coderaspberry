import RPi.GPIO as g
from Classes.Logger import log_info


class PinManager:
    __mode=""
    __pin_num=0
    def __init__(self, isInput, PinNum):
        g.setwarnings(False)
        self.__pin_num = PinNum

        g.setmode(g.BCM)
        if isInput:
            self.__mode = "INPUT"
            g.setup(PinNum, g.IN, pull_up_down=g.PUD_DOWN)
        else:
            self.__mode = "OUTPUT"
            g.setup(PinNum, g.OUT)
        log_info("Pin {} set as {}".format(self.__pin_num,self.__mode))


    def output(self, val):
        if self.__mode == "OUTPUT":
            log_info("Pin {} output : {}".format(self.__pin_num,val))
            g.output(self.__pin_num, val)

    def input(self):
        if self.__mode == "INPUT":
            log_info("Pin {} input : {}".format(self.__pin_num,g.input(self.__pin_num)))
            return bool(g.input(self.__pin_num))
