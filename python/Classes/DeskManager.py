from Classes.Logger import log_info
from Classes import ThreadManager
from Classes import DataBase
from Classes import OrderManager
import time
import datetime


class DeskManager:
    __instance = None
    __database_manager = None
    __order_manager = None

    @staticmethod
    def getInstance():
        """ Static access method. """
        if DeskManager.__instance is None:
            DeskManager()
        return DeskManager.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if DeskManager.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            DeskManager.__instance = self
            self.__database_manager = DataBase.DataBase()
            self.__order_manager = OrderManager.OrderManager()

    def databaseWatcher(self):
        while 1:
            time.sleep(0.1)
            orders = self.__database_manager.select(["*"], ["php-orders"], ["`execution-done`=0"])
            if len(orders) != 0:
                log_info(str(orders))
                for order_it in orders:
                    log_info("Executing order : "+str(order_it))
                    if order_it[1].split(" ")[0] == "PC":
                        log_info("Order received")
                        self.__order_manager.pcControl(order_it[1].split(" ")[1])
                        order = "0";
                        if self.__order_manager.pcStatus():
                            order = "1"
                        if str(self.__database_manager.select(["*"],["pc-status"],None,"date DESC")[0][1]) != order:
                            log_info("Update PC Status")
                            self.__database_manager.insert("pc-status",["date","state"],[datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),order])
                    else:
                        log_info("Not receivable order")
                    log_info("Order treated")
                    id_line = "`order-time`='{}'".format(order_it[0].strftime("%Y-%m-%d %H:%M:%S"))
                    self.__database_manager.update("php-orders", ["execution-done"], ["1"], [id_line])
                    log_info("Order updated in database")
