from Classes.Logger import log_info
from Classes import PinManager
import time as t


class OrderManager:
    __on_off_manual_input = None
    __is_on_off_led = None
    __on_off_executer = None

    def __init__(self):
        self.__on_off_manual_input = PinManager.PinManager(True, 26)
        self.__is_on_off_led = PinManager.PinManager(True, 22)
        self.__on_off_executer = PinManager.PinManager(False, 20)

    def pcControl(self, order):
        if (order == "on" and self.__is_on_off_led.input() is False) or (order == "off" and self.__is_on_off_led.input() is True):
            log_info("Turning {}".format(order))
            self.__on_off_executer.output(True)
            t.sleep(0.1)
            self.__on_off_executer.output(False)
        elif order == "offHard" and self.__is_on_off_led.input() is True:
            log_info("Turn off hard")
            while self.__is_on_off_led.input() is True:
                self.__on_off_executer.output(True)
                t.sleep(0.1)
            self.__on_off_executer.output(False)

    def pcStatus(self):
        return self.__is_on_off_led.input()