import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

from Classes.Logger import log_info


class DataBase:
    __connection = None
    __cursor = None

    def __init__(self):
        try:
            self.__connection = mysql.connector.connect(host='localhost', database='database', user='process_access',
                                                        password='qlvUMEkEZJzVthKN')

            if self.__connection.is_connected():
                self.__cursor = self.__connection.cursor()
        except Error as e:
            print("Error while connecting to MySQL", e)

    def select(self, columns, tables, conditions=None, orderBy="", maxrow=-1):
        columnsStr = ",".join(self.sql_format(columns, "`"))
        tablesStr = ",".join(self.sql_format(tables, "`"))
        if conditions is None:
            conditions = ["1"]
        if orderBy != "":
            orderBy = " ORDER BY " + orderBy
        if maxrow != -1:
            maxrow = " LIMIT {}".format(maxrow)
        else:
            maxrow = ""

        conditionsStr = " WHERE " + " AND ".join(conditions) + orderBy + maxrow
        self.__cursor.execute("SELECT DISTINCT " + columnsStr + " FROM " + tablesStr + conditionsStr + ";")
        return self.__cursor.fetchall()

    def insert(self, table, columns, valuesArr):
        columns = self.sql_format(columns, "`")
        values = self.sql_format(valuesArr)
        columnsStr = ",".join(columns)
        valuesStr = ",".join(values)
        reqStr = "INSERT INTO `" + table + "` (" + columnsStr + ") VALUES (" + valuesStr + ");"
        log_info(reqStr)
        self.__cursor.execute(reqStr)
        self.__connection.commit()

    def update(self, table, columns, valuesArr, conditions=None, setNull=False):
        columns = self.sql_format(columns, "`")
        values = self.sql_format(valuesArr)
        reqStr = "UPDATE `" + table + "` SET "
        update = []
        for i in range(len(columns)):
            if (values[i] != "NULL") or setNull:
                update.append(columns[i] + "=" + values[i])
        if conditions is None:
            conditions = ["1"]
        reqStr = reqStr + " " + ", ".join(update) + " WHERE " + " AND ".join(conditions) + ";"
        self.__cursor.execute(reqStr)
        self.__connection.commit()

    def delete(self, table, conditions=None):
        if conditions is None:
            conditions = []
        reqStr = "DELETE FROM `" + table + "` "
        if len(conditions) > 0:
            conditions = ["1"]
        reqStr = reqStr + " WHERE " + " AND ".join(conditions) + ";"
        self.__cursor.execute(reqStr)
        self.__connection.commit()

    def sql_format(self, data, char="'"):
        dataFormat = []
        for str in data:
            if str == "NULL" or str == "*":
                dataFormat.append(str)
            else:
                dataFormat.append(char + str.replace("'", "\'") + char)
        return dataFormat

    def reinitialize(self):
        f = open("/home/pi/www-data/coderaspberry/php/assets/pivot.sql",'r')
        self.__cursor.execute(f.read())
        f.close()
