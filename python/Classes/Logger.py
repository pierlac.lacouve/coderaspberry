from datetime import datetime
import logging

logging.basicConfig(filename='/home/pi/pylogs/runtime.log', level=logging.DEBUG)


def log_info(message):
    logging.info(datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + ' : ' + message)
