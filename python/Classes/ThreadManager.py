import os
from threading import Thread, RLock


class ThreadManager():
    def __init__(self):
        self.threads = []
        self.verrou = RLock()
        self.lastThread = 0

    def addThread(self, scriptPath):
        self.threads.append(ScriptThread(scriptPath))

    def runThreads(self):
        for i in range(self.lastThread, len(self.threads)):
            thread_ic = self.threads[i]
            thread_ic.start()
        self.lastThread = len(self.threads)


class ScriptThread(Thread):

    def __init__(self, path):
        Thread.__init__(self)
        self.scriptPath = path

    def run(self):
        os.system("python3.7 {}".format(self.scriptPath))
